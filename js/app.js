//Code Js pour le tp
$(document).ready(function(){
    
    
    // Fonction ingrédient apparait quand souris passe dessus
    $('.pizza-type label').hover(

        function () {
            $(this).find(".description").show();
        },
        function () {
            $(this).find(".description").hide();
        }
    )

    
    //Fonction image nombre de part équivalant au chiffre
    $('.nb-parts input').on('keyup', function () {

        const pizza = $('<span class="pizza-pict"></span>');
        nbParts = +$(this).val();

        $(".pizza-pict").remove();

        for(let i = 0; i<nbParts/6 ; i++){

            $(this).after(pizza.clone().addClass("pizza-6"));
        }

        if(nbParts%6 !== 0) $('.pizza-pict').last().removeClass('pizza-6').addClass('pizza-' + nbParts%6);
        price();
    })

    
    //fonction next-step
    $('.next-step').click(function () {
        $('.infos-client').slideDown();
        $(this).hide();
    })

    
    //fonction autre ligne adresse
    const input = $('<br><input type="text"/>');
    $('.add').click(function () {
        $(this).before(input.clone());
    })
    
    

    //fonction affiche commande
    $('.done').click(function () {

        const name = $('#name').val();
        const thanks = $('<span>Merci ' + name + ' ! Votre commande sera livrée entre 30 et 45 minutes</span>');
        $('.main').slideUp();
        $('.headline').append(thanks);
    })
    
    
    
});